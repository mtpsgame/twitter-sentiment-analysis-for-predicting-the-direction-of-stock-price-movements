# Twitter Sentiment Analysis for Stock Market Prediction

In this project, tweets are gathered using the TwitterAPI library. The tweet gathering algorithm attempts to only collect tweets related to Tesla stock, which contain some form of emotion or opinion written in the first person. Gathered tweets are passed through a naive Bayes classifier, to mark each one as positive or negative. The percentage of positive tweets for each day is calculated.

Two time series are created, one for the daily historical price data of Tesla stock (which has a diff applied), and one for the daily percentage of positive tweets. Two LSTM networks are created, one taking just the price time series as input and the other taking both time series as input. Results over a range of LSTM architectures are compared.