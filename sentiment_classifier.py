from random import shuffle

import nltk
from nltk.corpus import twitter_samples
from textblob.classifiers import NaiveBayesClassifier


def create_daily_sentiment_analysis_stats(daily_tweets, classifier):
    """
    Runs each tweet of each day through the sentiment classifier, producing the percentage of positive tweets for each
    day.
    :param daily_tweets: A list of tuples, (date, [list_of_tweet_texts]).
    :param classifier: A classifier with two targets, 'pos' and 'neg'.
    :return: A list of tuples, (date, positive_percentage).
    """
    daily_sentiments = []

    for day in daily_tweets:
        num_positive_tweets = 0

        for tweet in day[1]:
            if classifier.classify(tweet) == 'pos':
                num_positive_tweets += 1

        positive_percentage = num_positive_tweets / len(day[1])
        daily_sentiments.append((day[0], positive_percentage))

    return daily_sentiments


def train_sentiment_classifier():
    """
    Trains a naive Bayes classify on a tweet corpus to classify tweets as positive or negative.
    :return: The trained naive Bayes classifier object.
    """
    nltk.download('twitter_samples')

    pos_tweets = [(tweet, 'pos') for tweet in twitter_samples.strings('positive_tweets.json')]
    neg_tweets = [(tweet, 'neg') for tweet in twitter_samples.strings('negative_tweets.json')]

    shuffle(pos_tweets)
    shuffle(neg_tweets)

    train_set = pos_tweets[:int(0.8 * len(pos_tweets))] + neg_tweets[:int(0.8 * len(neg_tweets))]
    test_set = pos_tweets[int(0.8 * len(pos_tweets)):] + neg_tweets[int(0.8 * len(neg_tweets)):]

    sentiment_classifier = NaiveBayesClassifier(train_set)

    accuracy = sentiment_classifier.accuracy(test_set)
    print("Sentiment classifier accuracy " + str(accuracy * 100) + "%")

    return sentiment_classifier
